"use strict";

// BOM - browser object model
// console.log(window);
// console.log(navigator);

// JSOM, CSSOM

// DOM - document object model
// console.log(document);
// console.log(document.body);
// console.log(document.body.childNodes); // NodeList []
// console.log(document.body.children); // HTMLCollection {}

// document.body.childNodes[0].data = "<p>first text node</p>";
// document.body.childNodes[0].remove();
// document.body.childNodes[0].remove();

// document.body.childNodes.forEach((el) => {
//    console.log(el);
// });
// document.body.children[0].children[0].remove();

// console.log(document.body.children[0].children[0].textContent);
// const childrenArray = Array.from(document.body.children);

// console.log(childrenArray);

// childrenArray.forEach((el) => {
//    console.log(el);
// });

// const elByTagName = document.getElementsByTagName("li");
// console.log(elByTagName);

// const elByClassName = document.getElementsByClassName("list-item");
// for (const iterator of elByClassName) {
//    console.log(iterator);
// }
// for (const iterator in elByClassName) {
//    console.log(iterator);
// }
// for (let index = 0; index < elByClassName.length; index++) {
//    console.log(elByClassName[index]);
// }
// const elByID = document.getElementById("list");
// console.log(elByID);

// const firstItem = document.getElementById("first-item");
// console.log(firstItem);

// const elQueryID = document.querySelector("#list");
// console.log(elQueryID);

// const elQueryAll = document.querySelectorAll(".list-item");
// console.log(elQueryAll);

// const elQuery = document.querySelector(".list-item");
// console.log(elQuery);

// console.log(elQuery.className, typeof elQuery.className);
// console.log(elQuery.classList, typeof elQuery.classList);

// const changeColor = confirm("change Color?");
// if (changeColor) {
//    elQuery.className += " first-el";
// }

// console.log(elQuery);
// elQuery.className += " first-el";
// console.log(elQuery.className);

// elQuery.classList.add("first-el-add");
// console.log(elQuery.classList);

// elQuery.classList.remove("first-el-add");
// console.log(elQuery.classList);

// elQuery.classList.toggle("first-el");
// console.log(elQuery.classList);

// elQuery.setAttribute("data-font-color", "#0f0");
// elQuery.setAttribute("data-target", "#list");
// elQuery.setAttribute("data-developer", "Marchenko Roman");
// console.log(elQuery.getAttribute("data-font-color"));

// console.log(elQuery.style);
// elQuery.style.color = elQuery.getAttribute("data-font-color");

// const getStyles = window.getComputedStyle;
// console.log(getStyles(elQuery));
// const styles = getStyles(elQuery);

// console.log(styles.fontWeight);
// elQuery.style.fontWeight = 700;

// elQuery.textContent += "<span>_123</span>";
// elQuery.innerText += "<span>_456</span>";
// elQuery.innerHTML += "<strong>_789</strong>";

// TASK 1
// const list = document.getElementById("list");
// console.log(list);
// const listItem = document.getElementsByClassName("list-item");
// console.log(listItem);
// const tagName = document.getElementsByTagName("li");
// console.log(tagName);
// const Selector = document.querySelector("li:nth-child(3)");

// console.log(`${Selector}`);
// console.log(`${{}}`);

// const allselector = document.querySelectorAll("li");

// console.log(`${allselector}`);
// console.log(allselector[0].innerText);

// console.log(Selector.innerText);
// console.log(Selector.innerHTML);
// console.log(list.outerHTML);

// console.log(typeof []);

// TASK 2
const biggerEl = document.querySelector(".bigger");

// biggerEl.className = biggerEl.className.replace("bigger", "active");

biggerEl.className = Array.from(biggerEl.classList)
   .filter((el) => el !== "bigger")
   .concat("active")
   .join(" ");
