"use strict";

// const root = document.getElementById("root");

// const paragr = document.createElement("p");

// paragr.textContent = "new tag";
// paragr.className = "text-block";
// paragr.style.fontSize = "24px";

// console.log(paragr);

// const span = document.createElement("span");
// span.textContent = "new span";

// paragr.append(span);
// root.append(paragr);

// document.write("text");
// document.write(paragr);

// root.append("new text");

// // inside
// root.append(paragr);
// root.prepend(paragr);
// // outside
// root.before(paragr);
// root.after(paragr);

// root.insertAdjacentElement("afterbegin", paragr);
// root.insertAdjacentHTML("afterend", "<span>new span</span>");
// root.insertAdjacentText("beforeend", "<span>new span</span>");

// const fragment = document.createDocumentFragment();
// const paragrArr = [];

// for (let i = 0; i < 10; i++) {
//    const newParagr = paragr.cloneNode(true);
//    newParagr.textContent += ` ${i + 1}`;
//    fragment.append(newParagr);
//    paragrArr.push(newParagr);
// }

// console.log(fragment);
// root.append(fragment);
// console.log(fragment);

// paragr.remove();
// console.log(paragr);
// console.log(paragrArr);

// paragrArr.length = 0;
// console.log(paragrArr);

// paragrArr.forEach((element) => {
//    element.remove();
// });

// root.textContent = "";
