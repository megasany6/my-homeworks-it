"use strict";

const str = "Some \n string \u{1F60D}"; // '', ``, ""
// console.log(str);

// console.log(str.length);
// console.log(str[0]);

// str[0] = "x";
// for (let index = 0; index < str.length; index++) {
//    console.log(str[index]);
// }

// console.log(str.toUpperCase());
// console.log(str.toLowerCase());
// console.log(str);

// console.log(str.indexOf("ome", 2));
// console.log(str.lastIndexOf("ome", 2));

// console.log(str.includes("omee"));
// console.log(str.startsWith("So"));
// console.log(str.endsWith("S"));

// console.log(str.slice());
// console.log(str.slice(3));
// console.log(str.slice(3, 8));

// console.log(str.substring(3, 8));
// console.log(str.substr(3, 5).toUpperCase());

// console.log("a" > "Z");

// console.log("a".charCodeAt(0));
// console.log("Z".charCodeAt(0));

// console.log(String.fromCodePoint(90));

// for (let i = 0; i < 4000; i++) {
//    console.log(String.fromCodePoint(i));
// }

// console.log("ab" > "aac");

// const str2 = "  zz  ";
// console.log(str2.trim());
// console.log(str2.trimLeft());
// console.log(str2.trimRight());

// console.log(str2.repeat(2));
// console.log(str2.replace("z", "ab"));
// console.log(str2.replaceAll("z", "ab"));

// // console.log(str2.split("z"));
// // console.log(typeof "z");

// const today = new Date();
// console.log(today);

// today.setFullYear(2020);
// console.log(today);

// const date1 = new Date("December 17, 1995 03:24:00");
// console.log(date1);

// console.log(Date.now());

// const date2 = new Date("1998-10-17T03:24:00");
// console.log(date2);

// console.log(date1 === date2);

// console.log(date1 - date2);

// console.log(today.getFullYear());
// console.log(today.getMonth() + 1);
// console.log(today.getTime());
// console.log(new Date(Date.UTC(96, 1, 2, 3, 4, 5)));
// console.log(new Date(1619255096750));
// console.log(new Date(Date.now()));
// TASK 1
// const repeat = function (strSearch, count) {
//    if (
//       typeof strSearch !== "string" ||
//       typeof count !== "number" ||
//       isNaN(count)
//    ) {
//       console.log("Error");
//       return;
//    }
//    let strResult = "";
//    for (let index = 0; index < count; index++) {
//       strResult += strSearch;
//    }

//    return strResult;
// };
// console.log(repeat("abc", 5));
// "zzz"+"zzz"+"zzz"+"zzz"

// console.log("b" + "a" + +"a" + "a");

// for (const value of "asdfghjkl") {
//    console.log(value);
// }

// TASK 2
// const capitalizeAndDoublify = function (str) {
//    let newStr = "";
//    for (const symbol of str) {
//       newStr += symbol.toUpperCase().repeat(2);
//    }
//    return newStr;
// };

// console.log(capitalizeAndDoublify("hello")); // "hello" --> "HHEELLLLOO"

// TASK 3
// const truncate = function (str, maxLength) {
//    let newStr = "";
//    if (str.length > maxLength) {
//       newStr = str.slice(0, maxLength - 3) + "...";
//    }
//    return newStr;
// };
// console.log(truncate("abcdef", 5));

// TASK 4

function storeHelper(str) {
   let newStr = "";
   let arr = str.split(",");

   for (let elem of arr) {
      newStr += elem + "-" + Math.floor(Math.random() * 10) + ", \n";
   }
   return newStr;
}

console.log(storeHelper("water,banana,black,tea,apple"));
// "water -2, \n banana - 3, black - 3, tea - 5, apple - 6"

console.log(["water", "banana", "black", "tea", "apple"].join());
console.log(["water", "banana", "black", "tea", "apple"][0]);
