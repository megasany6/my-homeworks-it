"use strict";

// const arr = [];
// const arr = [1, 2, 3, "sdf", true, function () {}, { name: "Roman" }, null];
// console.log(arr);
// console.log(arr[3]);
// console.log(arr.length);

// arr.push("end", "end2");
// console.log(arr);
// console.log(arr.pop());

// arr.unshift("end", "end2");
// console.log(arr);
// console.log(arr.shift());

// arr.length = 10;
// console.log(arr[9]);
// console.log(arr);
// arr[15] = 15;
// console.log(arr);

// for (const iterator of arr) {
//    console.log(iterator);
// }

// for (let index = 0; index < arr.length; index++) {
//    console.log(arr[index]);
// }
// delete arr[1];
// console.log(arr);

// arr.slice(1, 3);
// console.log(arr.slice(1, 2));
// console.log(arr);

// arr.splice(1, 2);
// console.log(arr.splice(1, 0, "new1", "new2", "new3"));
// console.log(arr);

// console.log(arr.includes(11));
// console.log([1, 2, 3, 4, 5, 2, 1].indexOf(1));
// console.log([1, 2, 3, 4, 5, 2, 1].lastIndexOf(2));

// console.log(arr.join("-"));
// console.log(arr.reverse());

// console.log([1, 2, 3].concat([1, 4, 5, 6], [7, 8, 9]));
// const arr2 = [1, 2, 3, 4];

// const arrCbFn = (value, index, arr) => {
//    arr[index] = value * 2;
// };

// arr2.forEach(arrCbFn);

// console.log(arr2);
// // console.log(typeof arr2);
// console.log(arr2.find((el) => el >= 3));
// console.log(arr2.findIndex((el) => el >= 3));

// const arr2 = ["1", 2, 3, 4, true];
// let res = arr2.filter((el) => el % 2 === 0);
// console.log(res);
// console.log(arr2);

// res = arr2.map((el) => typeof el);
// res = arr2.map((el) => el % 2 === 0);
// console.log(res);

// console.log(arr2.sort((a, b) => a - b));
// console.log(arr2.reduce((sum, el, index, arr) => sum + el, 0));
// console.log(arr2.reduceRight((sum, el, index, arr) => sum + el, 0));

// console.log(Array.isArray([]));
// console.log(Array.isArray(arr2));
// console.log(Array.isArray({}));

// TASK 1

// const days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
// 'Friday', 'Saturday', 'Sunday', ];

// for (let i = 0; i < days.length; i++) {
//     console.log(days[i]);
//     };

//     for (const elem of days) {
//         console.log(elem);
//     }

//     days.forEach((element) => {
//         console.log(element);
//     });

// const days = {
//    ua: [
//       "Понеділок",
//       "Вівторок",
//       "Середа",
//       "Четвер",
//       "П’ятниця",
//       "Субота",
//       "Неділя",
//    ],
//    ru: [
//       "Понедельник",
//       "Вторник",
//       "Среда",
//       "Четверг",
//       "Пятница",
//       "Суббота",
//       "Воскресенье",
//    ],
//    en: [
//       "Monday",
//       "Tuesday",
//       "Wednesday",
//       "Thursday",
//       "Friday",
//       "Saturday",
//       "Sunday",
//    ],
// };

// TASK 2
// function askUser(days) {
//    let lang = prompt("На каком язіке?", "ru");
//    lang = lang.toLowerCase();
//    while (!Object.keys(days).includes(lang)) {
//       lang = prompt("На каком язіке?");
//    }
//    days[lang].forEach((element) => {
//       console.log(element);
//    });
// }
// askUser(days);

// TASK 3
// function mergeArrays(...rest) {
//    rest.forEach((element, index, arr) => {
//       if (!Array.isArray(element)) {
//          console.log(`Error ${index + 1}`);
//          return;
//       }
//    });

//    return [].concat(...rest);
// }
// console.log(
//    mergeArrays(
//       [112, 23, 33, 45],
//       ["a", "b", "c", "a", "q"],
//       [123, "123", "sdf"]
//    )
// );

// TASK 4
// let storage = [
//    "apple",
//    "water",
//    "banana",
//    "pineapple",
//    "tea",
//    "cheese",
//    "coffee",
// ];

// function replaceItems(name, products) {
//    if (!storage.includes(name)) {
//       console.log("Такого товара нет");
//       return;
//    }
//    if (!Array.isArray(products)) {
//       console.log("Не массив");
//    }
//    storage.splice(storage.indexOf(name), 1, ...products);
// }

// replaceItems("apple", ["cat", "dog"]);
// console.log(storage);

function developerSkillInspector() {
   const skills = [];

   while (true) {
      let skill = prompt("Enter your skill");

      if (!skill && skills.length === 0) {
         continue;
      }
      if (!skill && skills.length !== 0) {
         break;
      }

      skills.push(skill);
   }

   console.log(skills);
}
developerSkillInspector();
