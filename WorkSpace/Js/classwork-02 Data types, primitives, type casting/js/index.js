"use strict";

// let myVar = +"33";

// myVar += +myVar; // myVar = myVar + myVar
// console.log(myVar);

// console.log(myVar.toString());

// console.log(parseInt("3.14az"));
// console.log(Number("777"));
// console.log(String(33));

// console.log(Number(true));
// console.log(String(false));

let myVar = parseFloat("3.14az");
// console.log(myVar);

// myVar = 1 == "1";
// console.log(myVar); // true

// myVar = 1 === "1";
// console.log(myVar); // false

// myVar = ((1 === "1") == 0) == 1;
// console.log(myVar);

// myVar = 1 > 0;
// console.log(myVar);

// myVar = 1 < 0;
// console.log(myVar);

// myVar = 1 >= 0;
// console.log(myVar);

// myVar = 1 <= 0;
// console.log(myVar);

// myVar = 1 !== 0;
// console.log(myVar);

// myVar = Boolean("0");
// console.log(myVar);

// myVar = !!" ";
// console.log(myVar);

// console.log(0 && 2);
// console.log(undefined && 2);
// console.log(null && 2);
// console.log("" && 2);
// console.log(![] && 2);

// console.log(3 && 2);

// let myVarAgain;

// myVar = myVarAgain || 3;
// console.log(myVar);

// myVar = (1 == "1" && 2 === "2") || 10;
// console.log(myVar);

/**
 * Задание
 *
 * Объяснить поведение каждое операции.
 */

let x, y, z;

// x = 6;
// y = 15;
// z = 4;
// console.log((z = --x - y * 5)); // -70

// x = 6;
// y = 15;
// z = 4;
// console.log((y /= x + (5 % z))); // 2.14...

// x = 6;
// y = 15;
// z = 4;
// let s = 6;
// console.log((s += y - x++ * z)); // s +=3 -> s = s + 3
// console.log(x);

// console.log(!!"false" == !!"true");
// console.log("true" == true);
// console.log("true" === true);
// console.log(NaN == 1);
// console.log(NaN == NaN);
// console.log(NaN === NaN);

// console.log(0 == "0");
// console.log(0 === "0");

// const first = false;
// const second = true;

// console.log(first == 0);
// console.log(second === 1);
