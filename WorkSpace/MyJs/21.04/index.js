"use strict";
const title = "Acer ...";
const oldPrice = 22999;
const newPrice = 19999;
const topRated = true;

const product = {
  title: "Acer ...",
  "old-Price": 19999,
  "new Price": 19999,
  opRated: true,
  discount: 0.15,
  getProductTitle: function () {
    return this.title;
  },
  getPriceWithDiscount() {
    return this["new price"] * this.discount;
  },
};

// console.log(product["title"]); //console.log(product["new Price"]);
// console.log(product.title);

product.count = 33;
// console.log(product);

// console.log(product.getProductTitle);
// console.log(product.getPriceWithDiscount);

// const product2 = {};

// for (const key in product) {
//   //   console.log(key);
//   profuct2[key] = product[key];
// }

const product2 = Object.assign({}, product);

// const product2 = product;
console.log(product);
console.log(product2);

product2.count = 10;
console.log(product);
console.log(product2);

//const product3 = Object.assign(product2, product)

const obj1 = { count: 6 };
const obj2 = { count: 3 }; //ставит крайний

console.log(Object.assign(obj1, obj2));

product2.title = "Mac Air";

console.log(product2.getProductTitle());

console.log(Object.keys(product));
console.log(Object.entries(product));
console.log(Object.values(product));
