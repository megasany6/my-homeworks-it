const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const ul = document.createElement("ul");
let root = document.getElementById("root");
root.append(ul);
try {
  books.forEach((item) => {
    if (Object.keys(item).length < 3) {
      if (item.hasOwnProperty("author") === false) {
        throw new Error("Author is missing");
      }
      if (item.hasOwnProperty("name") === false) {
        throw new Error("Name is missing");
      }
      if (item.hasOwnProperty("price") === false) {
        throw new Error("Price is missing");
      }
    }
  });
} catch (e) {
  console.log(e);
}

books.forEach((item) => {
  if (Object.keys(item).length === 3) {
    const li = document.createElement("li");

    for (const [key, value] of Object.entries(item)) {
      li.insertAdjacentText("beforeend", ` ${key}: ${value} //`);
    }

    ul.append(li);
  }
});
