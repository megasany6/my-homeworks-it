"use strict";
function createFilmEl(text, data = "", el = "p") {
    const element = document.createElement(el)
    element.textContent = `${text} ${data}`
    return element
}

fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response => response.json())
    .then(data => data.forEach(({ episodeId, name, openingCrawl, characters }) => {
        const films = {
            rootEl: document.getElementById("root"),
            containerEl: document.createElement("article"),
            listEl: document.createElement("ul"),
            titleEl: createFilmEl("Name:", name, "h2"),
            episodeEl: createFilmEl("Episode:", episodeId),
            descrEl: createFilmEl("Description:", openingCrawl),
            loaderEl: createFilmEl("Characters loading...")
        }
        films.titleEl.append(films.loaderEl)
        Promise.all(characters.map(characterUrl => {
            return fetch(characterUrl)
                .then(response => response.json())
                .then(({ name }) => {
                    const li = createFilmEl("", name, "li")
                    return li
                })
        }))
            .then(li => {
                films.listEl.append(...li)
                films.loaderEl.innerText = "Characters:"
            })
        films.containerEl.append(films.titleEl, films.listEl, films.episodeEl, films.descrEl)
        films.rootEl.append(films.containerEl)
    }))
    .catch(e => console.error(e.message))







