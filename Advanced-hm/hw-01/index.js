"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get nameInfo() {
        return this.name;
    }
    set nameInfo(newName) {
        this.name = newName;
    }
    get ageInfo() {
        return this.age;
    }
    set ageInfo(newAge) {
        this.age = newAge;
    }
    get salaryInfo() {
        return this.salary;
    }

    set salaryInfo(newSalary) {
        this.salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }

    get salaryInfo() {
        return this.salary * 3;
    }
}

const prog = new Programmer("Alex", 33, 6000, "js");
const prog1 = new Programmer("Max", 26, 5000, "c#");
const prog2 = new Programmer("Ivanna", 34, 8000, "java");


console.log(prog);
console.log(prog1);
console.log(prog2);
console.log(prog.salaryInfo);